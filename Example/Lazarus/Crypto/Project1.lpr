program Project1;
{$MODE Delphi}
uses
  Forms, Interfaces,
  Unit1 in 'Unit1.pas' {FormCrypt},
  CnMD5 in '..\..\..\Source\CnMD5.pas',
  CnBase64 in '..\..\..\Source\CnBase64.pas',
  CnCRC32 in '..\..\..\Source\CnCRC32.pas',
  CnDES in '..\..\..\Source\CnDES.pas',
  CnSHA1 in '..\..\..\Source\CnSHA1.pas',
  CnSM3 in '..\..\..\Source\CnSM3.pas',
  CnSM4 in '..\..\..\Source\CnSM4.pas',
  CnAES in '..\..\..\Source\CnAES.pas',
  CnSHA2 in '..\..\..\Source\CnSHA2.pas',
  CnZUC in '..\..\..\Source\CnZUC.pas',
  CnSHA3 in '..\..\..\Source\CnSHA3.pas',
  CnTEA in '..\..\..\Source\CnTEA.pas';
{$R *.res}
begin
  Application.Initialize;
  Application.CreateForm(TFormCrypt, FormCrypt);
  Application.Run;
end.

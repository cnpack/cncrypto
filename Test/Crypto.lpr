{******************************************************************************}  
{                       CnPack For Delphi/C++Builder                           } 
{                     中国人自己的开放源码第三方开发包                         } 
{                   (C)Copyright 2001-2025 CnPack 开发组                       } 
{                   ------------------------------------                       } 
{                                                                              } 
{            本开发包是开源的自由软件，您可以遵照 CnPack 的发布协议来修        } 
{        改和重新发布这一程序。                                                } 
{                                                                              } 
{            发布这一开发包的目的是希望它有用，但没有任何担保。甚至没有        } 
{        适合特定目的而隐含的担保。更详细的情况请参阅 CnPack 发布协议。        } 
{                                                                              } 
{            您应该已经和开发包一起收到一份 CnPack 发布协议的副本。如果        } 
{        还没有，可访问我们的网站：                                            } 
{                                                                              } 
{            网站地址：https://www.cnpack.org                                  } 
{            电子邮件：master@cnpack.org                                       } 
{                                                                              } 
{******************************************************************************} 
                                                                                 
program Crypto;                                                           
                                                                          
{$MODE Delphi}                                                            
                                                                          
{$APPTYPE CONSOLE}                                                        
                                                                          
{$R *.res}                                                                
                                                                          
uses                                                                      
  SysUtils, Interfaces,                                                   
  Cn25519 in '..\Source\Cn25519.pas',                                     
  CnAEAD in '..\Source\CnAEAD.pas',                                       
  CnAES in '..\Source\CnAES.pas',                                         
  CnBase64 in '..\Source\CnBase64.pas',                                   
  CnBerUtils in '..\Source\CnBerUtils.pas',                               
  CnBigDecimal in '..\Source\CnBigDecimal.pas',                           
  CnBigNumber in '..\Source\CnBigNumber.pas',                             
  CnBigRational in '..\Source\CnBigRational.pas',                         
  CnBits in '..\Source\CnBits.pas',                                       
  CnCertificateAuthority in '..\Source\CnCertificateAuthority.pas',       
  CnChaCha20 in '..\Source\CnChaCha20.pas',                               
  CnComplex in '..\Source\CnComplex.pas',                                 
  CnConsts in '..\Source\CnConsts.pas',                                   
  CnContainers in '..\Source\CnContainers.pas',                           
  CnCRC32 in '..\Source\CnCRC32.pas',                                     
  CnDES in '..\Source\CnDES.pas',                                         
  CnDFT in '..\Source\CnDFT.pas',                                         
  CnDSA in '..\Source\CnDSA.pas',                                         
  CnECC in '..\Source\CnECC.pas',                                         
  CnFEC in '..\Source\CnFEC.pas',                                         
  CnFloat in '..\Source\CnFloat.pas',                                     
  CnFNV in '..\Source\CnFNV.pas',                                         
  CnHashMap in '..\Source\CnHashMap.pas',                                 
  CnInt128 in '..\Source\CnInt128.pas',                                   
  CnKDF in '..\Source\CnKDF.pas',                                         
  CnLattice in '..\Source\CnLattice.pas',                                 
  CnMath in '..\Source\CnMath.pas',                                       
  CnMatrix in '..\Source\CnMatrix.pas',                                   
  CnMD5 in '..\Source\CnMD5.pas',                                         
  CnNative in '..\Source\CnNative.pas',                                   
  CnOTP in '..\Source\CnOTP.pas',                                         
  CnOTS in '..\Source\CnOTS.pas',                                         
  CnPaillier in '..\Source\CnPaillier.pas',                               
  CnPDFCrypt in '..\Source\CnPDFCrypt.pas',                               
  CnPemUtils in '..\Source\CnPemUtils.pas',                               
  CnPoly1305 in '..\Source\CnPoly1305.pas',                               
  CnPolynomial in '..\Source\CnPolynomial.pas',                           
  CnPrime in '..\Source\CnPrime.pas',                                     
  CnRandom in '..\Source\CnRandom.pas',                                   
  CnRC4 in '..\Source\CnRC4.pas',                                         
  CnRSA in '..\Source\CnRSA.pas',                                         
  CnSecretSharing in '..\Source\CnSecretSharing.pas',                     
  CnSHA1 in '..\Source\CnSHA1.pas',                                       
  CnSHA2 in '..\Source\CnSHA2.pas',                                       
  CnSHA3 in '..\Source\CnSHA3.pas',                                       
  CnSM2 in '..\Source\CnSM2.pas',                                         
  CnSM3 in '..\Source\CnSM3.pas',                                         
  CnSM4 in '..\Source\CnSM4.pas',                                         
  CnSM9 in '..\Source\CnSM9.pas',                                         
  CnStrings in '..\Source\CnStrings.pas',                                 
  CnTEA in '..\Source\CnTEA.pas',                                         
  CnTree in '..\Source\CnTree.pas',                                       
  CnVector in '..\Source\CnVector.pas',                                   
  CnWideStrings in '..\Source\CnWideStrings.pas',                         
  CnZUC in '..\Source\CnZUC.pas',                                       
  CryptoTest in 'CryptoTest.pas';                                         
                                                                          
begin                                                                     
  try                                                                     
    TestCrypto;                                                           
  except                                                                  
    on E: Exception do                                                    
      Writeln(E.ClassName, ': ', E.Message);                              
  end;                                                                    
end.                                                                      
                                                                          
